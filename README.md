# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

#Docker
* create a version 5.2 rails project 
  docker run -it --rm --user "$(id -u):$(id -g)" \
    -v "$PWD":/usr/src/app -w /usr/src/app ruby:2.5.1 bash

  gem install rails 5.2
  rails new app --database=postgresql --skip-bundle
  exit
  cd app
  
* sudo chown -R $USER:$USER .*

* docker-compose run --rm app bundle install
* docker-compose run --rm app bundle exec rails generate rspec:install
* docker-compose run --rm app bundle exec rails db:create db:migrate
* docker-compose run --rm app bash
* EDITOR=nano bundle exec rails credentials:edit
* docker-compose run --rm app bundle exec yarn add jquery
* docker-compose run --rm app bundle exec rails c
* docker-compose run --rm app bundle exec rspec spec/services/exchange_service_spec.rb
* docker-compose run --rm app bundle exec rails g controller Exchanges index convert --no-controller-specs