Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'exchanges#index'
  get 'convert', to: 'exchanges#convert'
end
